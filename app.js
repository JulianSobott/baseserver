var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var game_network = require('./lib/game_network');
var game_manager = require('./lib/game_manager');
var websockets = require('./lib/websockets');

var indexRouter = require('./routes/index');
var confRouter = require('./routes/configuration');
var espRouter = require('./routes/esp');
var gameRouter = require('./routes/game');
var debugRouter = require('./routes/debug');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/conf', confRouter);
app.use('/api/esp', espRouter);
app.use('/api/game', gameRouter);
app.use('/api/debug', debugRouter);


//Everything from here on might be cached / 304 not modified
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);

app.get('/*', function (req, res, next) {
  res.setHeader('Last-Modified', (new Date()).toUTCString());
  next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

game_network.registerListener(game_manager.processRadio);
game_network.startReceiving(2);
game_network.prepareTransmission(3);
websockets.start(process.env.WS_PORT || '3001');

console.log("Starting LazerMaster");

module.exports = app;
