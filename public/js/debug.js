function updatePlayerLists(json) {
    let table = document.getElementById("playerlist");
    if (table) {
        while (table.rows.length > 0) {
            table.deleteRow(table.rows.length - 1);
        }

        let count = Math.max(json.registered.length, json.tobeready.length, json.active.length, json.tobeevaluated.length);

        for (let i = 0; i < count; i++) {
            let row = table.insertRow();
            let registered = row.insertCell();
            registered.innerText = json.registered.length > i ? json.registered[i] : "";
            registered.classList.add("fancy-cell", "column");
            let ready = row.insertCell();
            ready.innerText = json.tobeready.length > i ? json.tobeready[i] : "";
            ready.classList.add("fancy-cell", "column");
            let active = row.insertCell();
            active.innerText = json.active.length > i ? json.active[i] : "";
            active.classList.add("fancy-cell", "column");
            let eval = row.insertCell();
            eval.innerText = json.tobeevaluated.length > i ? json.tobeevaluated[i] : "";
            eval.classList.add("fancy-cell", "column");
        }


    }

}

function getPlayerLists() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updatePlayerLists(result.value);
            }
        }
    };
    http.open("GET", "/api/debug/player_lists", true);
    http.send(null);
}

function updatePlayerManagerPhase(value) {
    let cell = document.getElementById("state-playermanager");
    cell.innerText = value;
}

function updateGameManagerPhase(value) {
    let cell = document.getElementById("state-gamemanager");
    cell.innerText = value;
}

function updateGameState(value) {
    let cell = document.getElementById("state-game");
    cell.innerText = value;
    getCountdown();
}

function getStates() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updateGameState(result.value);
            }
        }
    };
    http.open("GET", "/api/debug/game_state", true);
    http.send(null);

    let http2 = new XMLHttpRequest();
    http2.onreadystatechange = function () {
        if (http2.readyState === 4 && http2.status === 200) {
            let result = JSON.parse(http2.responseText);
            if (result.result === 0) {
                updateGameManagerPhase(result.value);
            }
        }
    };
    http2.open("GET", "/api/debug/game_manager_phase", true);
    http2.send(null);

    let http3 = new XMLHttpRequest();
    http3.onreadystatechange = function () {
        if (http3.readyState === 4 && http3.status === 200) {
            let result = JSON.parse(http3.responseText);
            if (result.result === 0) {
                updatePlayerManagerPhase(result.value);
            }
        }
    };
    http3.open("GET", "/api/debug/player_manager_phase", true);
    http3.send(null);
}

function processMessage(msg) {
    if (msg.type === "player_lists") {
        updatePlayerLists(msg.value);
    } else if (msg.type === "player_manager_phase") {
        updatePlayerManagerPhase(msg.value);
    } else if (msg.type === "game_manager_phase") {
        updateGameManagerPhase(msg.value);
    } else if (msg.type === "game_state") {
        updateGameState(msg.value);
    } else {
        console.log("Unknown message: " + JSON.stringify(msg));
    }
}

function startWebsocket() {
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    let conn = new WebSocket('ws://' + location.hostname + ":3001");
    conn.onopen = function () {
        console.log("Websocket connected");
    };

    conn.onerror = function (error) {
        console.log("Websocket error: " + error)
    };

    conn.onclose = function (e) {
        console.log("Websocket closed: " + e);
        setTimeout(function () {
            refresh();
            startWebsocket();

        }, 5000);
    };

    conn.onmessage = function (message) {
        try {
            let json = JSON.parse(message.data);
            processMessage(json);
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ',
                message.data, e);
        }
    }
}

let countdownInterval;
let stopTime;

function setupTimer(remaining) {
    if (countdownInterval) {
        clearInterval(countdownInterval);
    }
    if (remaining > 0) {
        stopTime = Date.now() + remaining;
        countdownInterval = setInterval(function () {
            updateCountdownDisplay(stopTime - Date.now());
        }, 1000);
        updateCountdownDisplay(remaining);
    } else {
        updateCountdownDisplay(-1);
    }

}

function updateCountdownDisplay(time) {

    let container = document.getElementById("countdown-container");
    let cell = document.getElementById("countdown");
    if (time > 0) {
        let minutes = Math.floor(time / 1000 / 60);
        let seconds = Math.floor((time / 1000) % 60);
        cell.innerText = minutes + ":" + ('0' + seconds).slice(-2);
        container.style.visibility = "visible";
        container.style.display = "block";
    } else {
        container.style.visibility = "hidden";
        container.style.display = "none";
    }

}

function getCountdown() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            setupTimer(result.result);

        }
    };
    http.open("GET", "/api/game/remaining_time", true);
    http.send(null);
}

function refresh() {
    getPlayerLists();
    getStates();
    getCountdown();
}

function onLoad() {
    refresh();
    startWebsocket();
}

window.onload = onLoad;