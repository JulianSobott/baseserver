var express = require('express');
var router = express.Router();

var game_state = require('../lib/game_state');
var game_manager = require('../lib/game_manager');
var player_manager = require('../lib/player_manager');

router.get('/*', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next(); // http://expressjs.com/guide.html#passing-route control
});

router.get('/', function (req, res) {
    return res.send("{'result':0}");
});


router.get('/player_lists', function (req, res, next) {
    let response = {};
    let lists = player_manager.getPlayerLists();
    response.result = 0;
    response.value = lists;

    res.json((response));
});

router.get('/game_state', function (req, res, next) {
    let response = {};
    response.result = 0;
    response.value = game_state.getStateString();

    res.send(JSON.stringify(response));
});

router.get('/player_manager_phase', function (req, res, next) {
    let response = {};
    response.result = 0;
    response.value = player_manager.getPhaseString();

    res.json((response));
});

router.get('/game_manager_phase', function (req, res, next) {
    let response = {};
    response.result = 0;
    response.value = game_manager.getPhaseString();

    res.json((response));
});

router.get('/startTest', function (req, res, next) {
    let response = {};
    let result = game_manager.startTestGame();
    if (result) {
        response.result = 0;
    } else {
        response.result = 1;
    }
    res.json((response));
});


module.exports = router;
