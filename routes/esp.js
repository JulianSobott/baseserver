var express = require('express');
var router = express.Router();

var game_setup = require('../lib/game_setup');
var player_manager = require('../lib/player_manager');
var game_manager = require('../lib/game_manager');

router.get('/*', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next(); // http://expressjs.com/guide.html#passing-route control
});

router.get('/', function(req, res) {
    return res.send("{'result': 0}");
});


router.get('/register', function (req, res) {
    let id = req.query.player_id;
    if (!isNaN(id) && Number(id) >= 0 && Number(id) <= 9) {
        let result = player_manager.registerPlayer(Number(id));
        let response = {"result": (result ? 0 : 1)};
        res.json(response);
        let main_version = req.query.main_version;
        let port_expander_version = req.query.port_version;
        let detector_version = req.query.detector_version;
        if (main_version || port_expander_version || detector_version) {
            console.log("Connected versions: Main " + main_version + " Port Expander " + port_expander_version + " Detector Version " + detector_version);
        }
    } else {
        res.json({"result": 2});
    }


});

router.get('/set_ready',function (req,res){
    let id = req.query.player_id;
    if (!isNaN(id) && Number(id) >= 0 && Number(id) <= 9) {
        let result = player_manager.readyPlayer(Number(id));
        let response = {"result": (result ? 0 : 1)};
        res.json(response);
    } else {
        res.json({"result": 2});
    }

});

router.get('/get_setup',function (req,res){
    let id = req.query.player_id;
    let response;
    if (game_manager.isReady() && (isNaN(id) || player_manager.isRegistered(Number(id)))) {
        response = game_setup.setup;
        response.result = 0;
    } else {
        response = {"result": 1};
    }
    res.json(response);
});

router.post('/submit_result',function (req,res){
    let id = req.query.player_id;
    let result = 0;
    if (!isNaN(id) && Number(id) >= 0 && Number(id) <= 9) {
        if (game_manager.isEvaluating()) {
            game_manager.handleResult(Number(id), req.body);
        } else {
            result = 1;
        }

    } else {
        result = 2;
    }
    res.json({"result": result});

});

module.exports = router;
