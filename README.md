## Basis Server
NodeJS server program handling game configuration and evaluation. Communicates with ESPs as well as configuration browser


## Project Setup
### For development on PC
- Make sure you have NodeJS (LTS10 dubnium) and npm installed
- Install https://github.com/maxanier/WiringPi-Sim.git (clone + `make && sudo make install`)
- Clone https://github.com/jdrucey/node-rcswitch-gpiomem3.git into `../node-rcswitch` and run `npm install --unsafeperm`
- Run ´npm install´ here
- Start with ´node bin/www´

### On Raspberry
- Make sure you have NodeJS (LTS10 dubnium) and npm installed
- Install `wiringpi` (`sudo apt install wiringpi`)
- Clone https://github.com/jdrucey/node-rcswitch-gpiomem3.git into `../node-rcswitch` and run ´npm install --unsafeperm´
- Run `npm install` here
- Install RF transmitter and receiver at appropriate pins (Transmitter: 11 (WiringPi: 0) and Receiver: 13 (WiringPi: 2))
- Start with `node bin/www`

## General

- IDs are usually integer strings in this project as they are used as object properties


## Lazermaster Raspberry Setup
The Lazermaster Raspberry provides a hotspot and DHCP server to allow ESPs and other clients to connect to it.  
### Properties
- Ip Address: `192.168.1.1`
- Hostname: `lazermaster`
- Mapped domain: `www.lazermaster.de` `lazermaster.de`
- Node port: `80`
- Websocket: `8001`

### Setup
- Install raspbian stretch
- Setup both eth0 and wlan0, setup dnsmasq (with the properties above), setup hostapd (e.g. like [here](https://www.elektronik-kompendium.de/sites/raspberry-pi/2002171.htm), skip the routing part)
- Eth0 is optional but makes allows connecting from another network
- Add 
```
no-hosts
addn-hosts=/etc/dnsmasq.hosts
```
to `/etc/dnsmasq.conf`
- Add `192.168.1.1 www.lazermaster.de lazermaster.de` to `/etc/dnsmasq.hosts`
- Add `up /sbin/iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 80 -j REDIRECT --to-port 3000` to `/etc/network/interfaces`
