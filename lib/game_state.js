var game_state = this;

var websockets = require('../lib/websockets');
let game_setup = require('../lib/game_setup');


const STATE = {"UNPREPARED": 0, "RUNNING": 1, "FINISHED": 2};
let scoreboard_raw;
let scoreboard_cached;
let stoptime = null;
let stopscore = null;
let currentMaxScore = 0;

/**
 * ID (String) to player object map
 */
let player_map;
let player_count;
let current_state = STATE.UNPREPARED;

let sendScoreboard = function () {
    if (scoreboard_cached) {
        let msg = {"type": "scoreboard"};
        msg.value = {"score": scoreboard_cached, "team": game_setup.isTeamMode()};

        websockets.broadcast(JSON.stringify(msg));
    }
};

let sendState = function () {
    let msg = {"type": "game_state"};
    msg.value = game_state.getStateString();
    websockets.broadcast(JSON.stringify(msg));
};

game_state.getStateString = function () {
    return Object.keys(STATE)[current_state];
};

game_state.isRunning = function () {
    return current_state === STATE.RUNNING;
};

game_state.isFinished = function () {
    return current_state === STATE.FINISHED;
};

game_state.isNotStarted = function () {
    return current_state === STATE.UNPREPARED;
};

game_state.startGame = function (list, scorelimit, timelimit) {
    scoreboard_raw = {};
    player_map = list;
    player_count = Object.keys(player_map).length;
    current_state = STATE.RUNNING;
    stoptime = timelimit ? Date.now() + timelimit : null;
    stopscore = scorelimit;
    updateScoreboard();
    sendState();
};


game_state.getRemainingTime = function () {
    return stoptime ? stoptime - Date.now() : -1;
};


game_state.tick = function () {
    if (stopscore && currentMaxScore > stopscore) {
        console.log("Score limit reached");
        return false;
    }
    if (stoptime && Date.now() > stoptime) {
        console.log("Time's up");
        return false;
    }
    return true;
};


game_state.stopGame = function (list) {
    current_state = STATE.FINISHED;
    sendState();
    sendResultText();
};

let checkID = function (id) {
    if (!player_map.hasOwnProperty(id)) {
        console.warn("Received id that is not active " + id);
        return false;

    }
    return true;
};

game_state.processHit = function (signal) {
    if (!game_state.isRunning()) return;
    let offender = '' + signal.offenderID;
    let kills = signal.kills;
    let victim = '' + signal.victimID;
    let assistID = '' + signal.hasAssist ? signal.assistID : null;
    let assists = signal.hasAssist ? signal.assists : null;
    if (offender !== undefined && kills !== undefined && victim !== undefined) {
        if (!checkID(offender)) return;
        if (!checkID(victim)) return;
        if (!scoreboard_raw[victim]) {
            scoreboard_raw[victim] = {};
        }
        let victim_board = scoreboard_raw[victim];
        if (!victim_board.killed_by) {
            victim_board.killed_by = {};
        }
        victim_board.killed_by[offender] = kills;
        if (assistID) {
            if (!victim_board.assisted_by) {
                victim_board.assisted_by = {};
            }
            victim_board.assisted_by[assistID] = assists;
        }
    } else {
        console.warn("Invalid hit signal " + signal);
    }
    updateScoreboard();
};

game_state.processDeath = function (signal) {
    if (!game_state.isRunning()) return;

    let victim = '' + signal.victimID;
    let deaths = signal.deaths;
    if (victim !== undefined && deaths !== undefined) {
        checkID(victim);
        if (!scoreboard_raw[victim]) {
            scoreboard_raw[victim] = {};
        }
        scoreboard_raw[victim].deaths = deaths;
    }
    updateScoreboard();
};

game_state.processResult = function (id, result) {
    if (!game_state.isFinished()) return;
    //If all players have submitted their results the entire scoreboard should be overriden
    //Insert player deaths
    if (!checkID(id)) return;
    console.log(JSON.stringify(result));
    let player_row = result[Number(id)];
    if (player_row) {
        let deaths = player_row[2];//Third value is deaths
        let modePoints = player_row[3]; //Fourth value is game mode points
        if (!scoreboard_raw[id]) {
            scoreboard_raw[id] = {};
        }
        scoreboard_raw[id].deaths = deaths;
        scoreboard_raw[id].modePoints = modePoints;
    } else {
        console.log("Cannot find player's own result row");
    }
    for (let pid in player_map) { //Only process rows related to players that are actually present
        if (!player_map.hasOwnProperty(pid)) continue;
        let row = result[Number(pid)];
        if (row) {
            if (!scoreboard_raw[id].killed_by) {
                scoreboard_raw[id].killed_by = {}
            }
            scoreboard_raw[id].killed_by[pid] = row[0]; //First value is kills
            if (!scoreboard_raw[id].assisted_by) {
                scoreboard_raw[id].assisted_by = {};
            }
            scoreboard_raw[id].assisted_by[pid] = row[1]; //Second value is assists
        } else {
            console.log("Missing result row for player " + pid + " of player " + id);
        }
    }
    updateScoreboard();
    sendResultText();
};

let sendResultText = function () {
    let msg = {"type": "result"};
    msg.value = game_state.getResultText();

    websockets.broadcast(JSON.stringify(msg));
};

game_state.getResultText = function () {
    if (!game_state.isFinished() || scoreboard_raw === undefined) return "Not finished";
    updateScoreboard();
    let gamemode = game_setup.setup.gameMode;
    let team = game_setup.isTeamMode();

    if (team) {
        let score1;
        let score2;

        for (let [id, player] of Object.entries(scoreboard_raw)) {
            checkID(id);
            let score = gamemode === "TeamDeathMatch" ? scoreboard_cached[id].kills : player.modePoints;
            if (player.team === 1) {
                score2 += score;
            } else {
                score1 += score;
            }
        }
        if (score1 === score2) {
            return "Tie with " + score1 + " points";
        } else {
            return "Team " + (score1 > score2 ? "1" : "2") + " won with " + Math.max(score1, score2) + " vs " + Math.min(score1, score2) + " points";
        }
    } else {
        let best_score = -1;
        let best_player = "Nobody";
        for (let [id, player] of Object.entries(scoreboard_raw)) {
            checkID(id);
            let score = gamemode === "FreeForAll" ? scoreboard_cached[id].kills : player.modePoints;
            if (score > best_score) {
                best_player = player_map[id].name;
                best_score = score;
            } else if (score === best_score) {
                best_player = best_player + " and " + player_map[id].name;
            }
        }
        return best_player + " won with " + best_score + " points";

    }
};

let calculateScore = function (kills, assists) {
    return kills * game_setup.setup.killMultiplier + assists * game_setup.setup.assistMultiplier;
};

let updateScoreboard = function () {
    if (scoreboard_raw === undefined || player_map === undefined) {
        scoreboard_cached = undefined;
        return;
    }
    scoreboard_cached = {};
    for (let [id, value] of Object.entries(player_map)) {
        scoreboard_cached[id] = {
            "name": value.name,
            "kills": 0,
            "assists": 0,
            "deaths": 0,
            "score": 0,
            "team": value.team
        };
    }
    for (let [id, value] of Object.entries(scoreboard_raw)) {
        checkID(id);
        scoreboard_cached[id].deaths = value.deaths;
        scoreboard_cached[id].score = value.modePoints;
        if (value.killed_by) {
            for (let [kill_id, kills] of Object.entries(value.killed_by)) {
                if (kill_id >= player_count) continue;
                scoreboard_cached[kill_id].kills += kills;
            }
        }
        if (value.assisted_by) {
            for (let [ass_id, assists] of Object.entries(value.assisted_by)) {
                if (ass_id >= player_count) continue;
                scoreboard_cached[ass_id].assists += assists;
            }
        }
    }
    let maxscore = 0;
    for (let [id, value] of Object.entries(scoreboard_cached)) {
        value.score += calculateScore(value.kills, value.assists);
        if (value.score > maxscore) {
            maxscore = value.score;
        }
    }
    currentMaxScore = maxscore;
    sendScoreboard();
};

game_state.getScoreBoard = function (forceUpdate) {
    if (forceUpdate) {
        updateScoreboard();
    }
    return scoreboard_cached;
};


module.exports = game_state;