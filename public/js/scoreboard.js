function updateScoreboard(json) {
    let table = document.getElementById("scoreboard");
    if (table) {
        while (table.rows.length > 0) {
            table.deleteRow(table.rows.length - 1);
        }
        var sortable = [];
        Object.keys(json).forEach(function (key) {
            sortable.push([json[key], json[key].score]);
        });

        sortable.sort(function (a, b) {
            return b[1] - a[1];
        });

        for (let i in sortable) {
            let entry = sortable[i][0];
            let row = table.insertRow();
            let name = row.insertCell();
            name.innerText = entry.name;
            name.classList.add("fancy-cell", "column1");
            let kills = row.insertCell();
            kills.innerText = entry.kills;
            kills.classList.add("fancy-cell", "column2");
            let assists = row.insertCell();
            assists.innerText = entry.assists;
            assists.classList.add("fancy-cell", "column3");
            let deaths = row.insertCell();
            deaths.innerText = entry.deaths;
            deaths.classList.add("fancy-cell", "column4");
            let score = row.insertCell();
            score.innerText = entry.score;
            score.classList.add("fancy-cell", "column5");
        }


    }

}

function getScoreboard() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updateScoreboard(result.value.score);
            }
        }
    };
    http.open("GET", "/api/game/scoreboard", true);
    http.send(null);
}

function processMessage(msg) {
    if (msg.type === "scoreboard") {
        updateScoreboard(msg.value.score);
    }
}

function startWebsocket() {
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    let conn = new WebSocket('ws://' + location.hostname + ":3001");
    conn.onopen = function () {
        console.log("Websocket connected");
    };

    conn.onerror = function (error) {
        console.log("Websocket error: " + error)
    };

    conn.onclose = function (e) {
        console.log("Websocket closed: " + e);
        setTimeout(function () {
            getScoreboard();
            startWebsocket();

        }, 5000);
    };

    conn.onmessage = function (message) {
        try {
            let json = JSON.parse(message.data);
            processMessage(json);
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ',
                message.data, e);
        }
    }
}

function onLoad() {
    getScoreboard();
    startWebsocket();
}

window.onload = onLoad;