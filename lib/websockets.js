let websockets = this;
let WebSocket = require("ws");


let processMessage = function (conn, text) {

};


websockets.start = function (port) {
    ws = new WebSocket.Server({port: port});
    ws.on('connection', function (ws) {
        ws.on('message', function (msg) {
            processMessage(this, msg);
        });
    })
};

websockets.broadcast = function (msg) {
    ws.clients.forEach(function (client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(msg);
        }
    })
};


module.exports = websockets;