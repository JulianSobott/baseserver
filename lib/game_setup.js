var game_setup = this;
var fs = require('fs');
var zschema = require('z-schema');
var jsonschema = require('jsonschema');
var validator2 = new jsonschema.Validator();
var validator = new zschema({});
var websockets = require('../lib/websockets');

let filename;
if (fs.existsSync(__dirname + "/../conf/last_setup.json")) {
    filename = "last_setup";
} else {
    filename = "default_setup";
}
fs.readFile(__dirname + "/../conf/" + filename + ".json", function (err, data) {
   if(err){
       throw err;
   }
   game_setup.setup=JSON.parse(data);
});
fs.readFile(__dirname + "/../schema/setup.schema.json", function (err, data) {
    if(err){
        throw err;
    }
    game_setup.schema=JSON.parse(data);
});

game_setup.loadTemplate = function (name) {
    fs.readFile(__dirname + "/../conf/" + name + "_setup.json", function (err, data) {
        if (err) {
            throw err;
        }
        game_setup.setup = JSON.parse(data);
    });
};


game_setup.isValid =  function(){
    if(!game_setup.schema)return false;

    let result = validator2.validate(game_setup.setup, game_setup.schema);
    //let result = validator.validate(game_setup.setup,game_setup.schema);
    if (!result.valid) {
        console.log("Not valid " + JSON.stringify(result.errors));
        return false;
    }
    return true;
};

game_setup.send = function () {
    let msg = {"type": "config"};
    msg.value = game_setup.setup;
    websockets.broadcast(JSON.stringify(msg));
};

game_setup.store = function () {
    fs.writeFile(__dirname + "/../conf/last_setup.json", JSON.stringify(game_setup.setup), function (err) {
        if (err) {
            console.error("Failed to write last setup");
            console.error(err);
        }
    })
};

game_setup.isTeamMode = function () {
    return ["TeamDeathMatch", "Capture", "CaptureFlag"].indexOf(game_setup.setup.gameMode) >= 0;
};

module.export = game_setup;