var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/scoreboard', function (req, res, next) {
    res.render('scoreboard', {});
});

router.get('/debug', function (req, res, next) {
    res.render('debug', {});
});

router.get('/main', function (req, res, next) {
    res.render('main', {title: 'Lasertag'});
});


/**
 * Try to trick android clients into thinking it has internet access
 */
router.get('/generate_204', function (req, res, next) {
    res.status(204).send("");
});

router.get('/gen_204', function (req, res, next) {
    res.status(204).send("");
});


module.exports = router;
