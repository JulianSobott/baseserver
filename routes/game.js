var express = require('express');
var router = express.Router();
let game_state = require('../lib/game_state');
let game_manager = require('../lib/game_manager');
let player_manager = require('../lib/player_manager');
let game_setup = require('../lib/game_setup');



router.get('/*', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next(); // http://expressjs.com/guide.html#passing-route control
});

router.get('/', function (req, res) {
    return res.send('{"result":0}');
});

router.get('/ready', function (req, res) {
    let response = {};
    let result = game_manager.setReady();
    if (result) {
        response.result = 0;
    } else {
        response.result = 1;
    }
    res.json(response);
});

router.get('/scoreboard', function (req, res, next) {
    let response = {};
    let score = game_state.getScoreBoard(false);
    if (score) {
        response.result = 0;
        response.value = {"score": score, "team": game_setup.isTeamMode()};
    } else {
        response.result = 1;
    }
    res.json(response);
});

router.get('/start', function (req, res, next) {
    let response = {};
    let result = game_manager.startGame();
    if (result) {
        response.result = 0;
    } else {
        response.result = 1;
    }
    res.json(response);
});

router.get('/stop', function (req, res, next) {
    let response = {};
    let result = game_manager.finishGame();
    if (result) {
        response.result = 0;
    } else {
        response.result = 1;
    }
    res.json(response);
});


router.get('/remaining_time', function (req, res, next) {
    let t = game_manager.isRunning() ? game_state.getRemainingTime() : -1;
    let response = {"result": t};
    res.json(response);
});

router.get('/removeRegisteredPlayer', function (req, res, next) {
    let id = req.query.id;
    if (id) {
        if (game_manager.isRegistration()) {
            player_manager.removePlayer(Number(id));
            return res.json({"result": 0});
        }
    }
    res.json({"result": 1});
});

router.get('/result', function (req, res, next) {
    if (!game_state.isFinished()) {
        res.json({"result": 0, "value": "Still running"});
    } else if (!player_manager.allEvaluated()) {
        res.json({"result": 1, "value": "Still evaluating"});
    } else {
        res.json({"result": 0, "value": game_state.getResultText()});
    }
});



module.exports = router;
