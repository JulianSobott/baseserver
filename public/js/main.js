function setReady() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
        }
    };
    http.open("GET", "/api/game/ready", true);
    http.send(null);
}

function start() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
        }
    };
    http.open("GET", "/api/game/start", true);
    http.send(null);
}


function getConfig() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updateConfig(result.value);
            }
        }
    };
    http.open("GET", "/api/conf/", true);
    http.send(null);
}

function changePlayer(id, value) {
    var http = new XMLHttpRequest();
    http.open("POST", "/api/conf/player/set/" + id);
    http.setRequestHeader("Content-Type", "application/json");

    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {

        }
    };
    console.log(value);
    http.send(JSON.stringify(value));
}


let countdownInterval;
let stopTime;

function setupTimer(remaining) {
    if (countdownInterval) {
        clearInterval(countdownInterval);
    }
    if (remaining > 0) {
        stopTime = Date.now() + remaining;
        countdownInterval = setInterval(function () {
            updateCountdownDisplay(stopTime - Date.now());
        }, 1000);
        updateCountdownDisplay(remaining);
    } else {
        updateCountdownDisplay(-1);
    }

}

function updateCountdownDisplay(time) {
    let container = document.getElementById("countdown-container");
    let cell = document.getElementById("countdown");
    if (time > 0) {
        let minutes = Math.floor(time / 1000 / 60);
        let seconds = Math.floor((time / 1000) % 60);
        cell.innerText = minutes + ":" + ('0' + seconds).slice(-2);
        container.style.visibility = "visible";
        container.style.display = "block";
    } else {
        container.style.visibility = "hidden";
        container.style.display = "none";
    }
}

function getCountdown() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            setupTimer(result.result);

        }
    };
    http.open("GET", "/api/game/remaining_time", true);
    http.send(null);
}

function updateConfig(config) {
    let opts = document.getElementsByClassName("config-opt");
    for (let i = 0; i < opts.length; i++) {
        let element = opts[i];
        let id = opts[i].id.replace("config-", "");
        let isBool = element.type && element.type === 'checkbox';
        let isNum = element.type && element.type === 'number';
        if (config[id] === undefined) {
            console.warn("Undefined config option " + id);
        } else if (isBool) {
            element.checked = config[id];
        } else {
            element.value = config[id];
        }
    }

    let playertable = document.getElementById("config-players");
    while (playertable.rows.length > 0) {
        playertable.deleteRow(playertable.rows.length - 1);
    }
    var sortable = Object.values(config.players);

    sortable.sort(function (a, b) {
        return a["id"] - b["id"];
    });

    for (let i = 0; i < sortable.length; i++) {
        let p = sortable[i];
        let row = playertable.insertRow();
        row.player = p;
        let id = row.insertCell();
        id.innerText = p["id"];
        id.classList.add("fancy-cell", "column1");
        let namecell = row.insertCell();
        namecell.classList.add("fancy-cell", "column2");
        let name = document.createElement("input");
        name.classList.add("name-input");
        name.type = "text";
        name.value = p["name"];
        name.maxLength = 10;
        namecell.appendChild(name);
        name.addEventListener("change", function () {
            row.player.name = name.value;
            changePlayer(row.player["id"], row.player);
        });

        let teamcell = row.insertCell();
        teamcell.classList.add("fancy-cell", "column3");
        let team = document.createElement("select");
        for (let j = 0; j < 2; j++) {
            let opt = document.createElement("option");
            opt.value = j;
            opt.innerText = j;
            team.appendChild(opt);
        }
        team.value = p["team"];
        teamcell.appendChild(team);
        team.addEventListener("change", function () {
            row.player.team = Number(team.value);
            changePlayer(row.player["id"], row.player);
        });

        let rm = row.insertCell();
        rm.classList.add("fancy-cell", "column4");
    }

}

function changeConfig(configid) {
    let element = document.getElementById("config-" + configid);
    let isBool = element.type && element.type === 'checkbox';
    let isNum = element.type && element.type === 'number';
    let newvalue;
    if (isBool) {
        newvalue = element.checked;
    } else if (isNum) {
        newvalue = Number(element.value);
    } else {
        newvalue = element.value;
    }

    var http = new XMLHttpRequest();
    http.open("POST", "/api/conf/set/" + configid);
    http.setRequestHeader("Content-Type", "application/json");

    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let res = JSON.parse(http.responseText);
            if (res.newvalue) {
                if (isBool) {
                    element.checked = res.newvalue;
                } else {
                    element.value = res.newvalue;
                }
            }
        }
    };
    http.send(JSON.stringify({"value": newvalue}));
}

function prepareConfig() {
    let opts = document.getElementsByClassName("config-opt");
    for (let i = 0; i < opts.length; i++) {
        let id = opts[i].id.replace("config-", "");
        opts[i].addEventListener("change", function () {
            changeConfig(id);
        });
    }
}

function removeRegisteredPlayer(id) {
    var http = new XMLHttpRequest();
    http.open("GET", "/api/game/removeRegisteredPlayer?id=" + id, true);
    http.send(null);
}

function updatePlayerLists(json) {
    let regtable = document.getElementById("registered-playerlist");
    if (regtable) {
        while (regtable.rows.length > 0) {
            regtable.deleteRow(regtable.rows.length - 1);
        }

        let row = regtable.insertRow();

        for (let i = 0; i < json.registered.length; i++) {
            let cell = row.insertCell();
            cell.innerText = json.registered[i];
            cell.classList.add("fancy-cell", "column");
            cell.addEventListener("click", function () {
                removeRegisteredPlayer(json.registered[i])
            });
        }

    }

    let readytable = document.getElementById("ready-playerlist");
    if (readytable) {
        while (readytable.rows.length > 0) {
            readytable.deleteRow(readytable.rows.length - 1);
        }

        let row = readytable.insertRow();

        for (let i = 0; i < json.tobeready.length; i++) {
            let cell = row.insertCell();
            cell.innerText = json.tobeready[i];
            cell.classList.add("fancy-cell", "column");
        }
        let startbutton = document.getElementById("startbutton");
        startbutton.disabled = json.tobeready.length !== 0;
    }

    let evaluatingtable = document.getElementById("evaluating-playerlist");
    let evaluatingtabletitle = document.getElementById("evaluating-playerlist-title");

    if (evaluatingtable) {
        while (evaluatingtable.rows.length > 0) {
            evaluatingtable.deleteRow(evaluatingtable.rows.length - 1);
        }

        if (json.tobeevaluated.length === 0) {
            evaluatingtable.style.display = "none";
            evaluatingtabletitle.style.display = "none";

        } else {
            evaluatingtable.style.display = "";
            evaluatingtabletitle.style.display = "";


            let row = evaluatingtable.insertRow();

            for (let i = 0; i < json.tobeevaluated.length; i++) {
                let cell = row.insertCell();
                cell.innerText = json.tobeevaluated[i];
                cell.classList.add("fancy-cell", "column");
            }
        }


    }

}

function getPlayerLists() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updatePlayerLists(result.value);
            }
        }
    };
    http.open("GET", "/api/debug/player_lists", true);
    http.send(null);
}

function fillScoreboardTable(table, players) {
    while (table.rows.length > 0) {
        table.deleteRow(table.rows.length - 1);
    }


    players.sort(function (a, b) {
        return b.score - a.score;
    });

    for (let i in players) {
        let entry = players[i];
        let row = table.insertRow();
        let name = row.insertCell();
        name.innerText = entry.name;
        name.classList.add("fancy-cell", "column1");
        let kills = row.insertCell();
        kills.innerText = entry.kills;
        kills.classList.add("fancy-cell", "column2");
        let assists = row.insertCell();
        assists.innerText = entry.assists;
        assists.classList.add("fancy-cell", "column3");
        let deaths = row.insertCell();
        deaths.innerText = entry.deaths;
        deaths.classList.add("fancy-cell", "column4");
        let score = row.insertCell();
        score.innerText = entry.score;
        score.classList.add("fancy-cell", "column5");
    }
}

function updateScoreboard(json, team) {
    let teamBoards = document.getElementsByClassName("scoreboard-holder-team");
    let singleBoards = document.getElementsByClassName("scoreboard-holder-single");
    for (let i = 0; i < teamBoards.length; i++) {
        teamBoards[i].style.display = team ? "" : "none";
    }
    for (let i = 0; i < singleBoards.length; i++) {
        singleBoards[i].style.display = team ? "none" : "";
    }
    let players = Object.values(json);

    if (team) {
        let team1Tables = document.getElementsByClassName("scoreboard-team1");
        for (let i = 0; i < team1Tables.length; i++) {
            let table = team1Tables[i];
            fillScoreboardTable(table, players.filter(function (el) {
                return el.team === 0;
            }));
        }

        let team2Tables = document.getElementsByClassName("scoreboard-team2");
        for (let i = 0; i < team2Tables.length; i++) {
            let table = team2Tables[i];
            fillScoreboardTable(table, players.filter(function (el) {
                return el.team === 1;
            }));
        }
    } else {
        let tables = document.getElementsByClassName("scoreboard");
        for (let i = 0; i < tables.length; i++) {
            let table = tables[i];
            fillScoreboardTable(table, players);
        }

    }
}

function getScoreboard() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                updateScoreboard(result.value.score, result.value.team);
            }
        }
    };
    http.open("GET", "/api/game/scoreboard", true);
    http.send(null);
}

function requestPhase() {
    let http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result === 0) {
                activatePhase(result.value);
            }
        }
    };
    http.open("GET", "/api/debug/game_manager_phase", true);
    http.send(null);
}

function updateResult(text) {
    let span = document.getElementById("result-text");
    span.innerText = text;
}

function getResult() {
    let http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === 4 && http.status === 200) {
            let result = JSON.parse(http.responseText);
            if (result.result >= 0) {
                updateResult(result.value);
            }
        }
    };
    http.open("GET", "/api/game/result", true);
    http.send(null);
}

function activatePhase(phase) {
    let setup = document.getElementById("setup");
    let ready = document.getElementById("ready");
    let running = document.getElementById("running");
    let eval = document.getElementById("evaluation");
    let active;
    switch (phase) {
        case "REGISTRATION":
            active = setup;
            break;
        case "READY":
            active = ready;
            break;
        case "RUNNING":
            active = running;
            getCountdown();
            break;
        case "EVALUATION":
            active = eval;
            break;
        default:
            active = setup;
    }
    setup.style.display = "none";
    ready.style.display = "none";
    running.style.display = "none";
    eval.style.display = "none";
    active.style.display = "block";
}

function processMessage(msg) {
    switch (msg.type) {
        case "game_manager_phase":
            activatePhase(msg.value);
            break;
        case "player_lists":
            updatePlayerLists(msg.value);
            break;
        case "scoreboard":
            updateScoreboard(msg.value.score, msg.value.team);
            break;
        case "config":
            updateConfig(msg.value);
            break;
        case "result":
            updateResult(msg.value);
            break;
        default:
            console.log("Unknown message:" + JSON.stringify(msg));
    }
}

function startWebsocket() {
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    conn = new WebSocket('ws://' + location.hostname + ":3001");
    conn.onopen = function () {
        console.log("Websocket connected");
        let footer = document.getElementById("footer");
        footer.style.color = "black";
    };

    conn.onerror = function (error) {
        console.log("Websocket error: " + JSON.stringify(error));
        let footer = document.getElementById("footer");
        footer.style.color = "red";
    };

    conn.onclose = function (e) {
        console.log("Websocket closed: " + JSON.stringify(e));
        let footer = document.getElementById("footer");
        footer.style.color = "red";
        setTimeout(function () {
            refresh();
            startWebsocket();

        }, 1000);
    };

    conn.onmessage = function (message) {
        try {
            let json = JSON.parse(message.data);
            processMessage(json);
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ',
                message.data, e);
        }
    };
    console.log("Starting WS");
    let footer = document.getElementById("footer");
    footer.style.color = "red";
}


function refresh() {
    requestPhase();
    getPlayerLists();
    getScoreboard();
    getConfig();
    getCountdown();
    getResult();
}

function onLoad() {
    refresh();
    prepareConfig();
    setTimeout(startWebsocket, 1000);
}

function onClose() {
    conn.close();
}

window.onload = onLoad;
window.onbeforeunload = onClose;