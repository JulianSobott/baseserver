var game_network = this;

var rcswitch = require('node-rcswitch');


const SIG_TYPE = {"UNKNOWN": -1, "SETUP_FINISHED": 5, "HIT": 2, "DEATH": 3, "TIMER_STARTED": 1, "GAME_OVER": 4};
game_network.SIG_TYPE = SIG_TYPE;
const SIG_SETUP_FINISHED = 5;
const SIG_TIMER_STARTED = 1;
const SIG_HIT = 2;
const SIG_DEATH = 3;
const SIG_GAME_OVER = 4;

const typeOffs = 0;
const victimOffs = 3;
const killOffs = 7;
const assistOffs = 19;
const offenderIDOffs = 15;
const hasAssistOffs = 26;
const assistIDOffs = 27;
const deathOffs = 7;
const parityOffs = 31;

const typeMask = 0x07;            // 0000 0000 0000 0000 0000 0000 0000 0111;
//type hit
const victimIDMask = 0x078;       // 0000 0000 0000 0000 0000 0000 0111 1000;
const killsMask = 0x07F80;        // 0000 0000 0000 0000 0111 1111 1000 0000;
const offenderIDMask = 0x078000;  // 0000 0000 0000 0111 1000 0000 0000 0000;
const assistsMask = 0x3F80000;    // 0000 0011 1111 1000 0000 0000 0000 0000;
const hasAssistMask = 0x4000000;  // 0000 0100 0000 0000 0000 0000 0000 0000;
const assistIDMask = 0x78000000;  // 0111 1000 0000 0000 0000 0000 0000 0000;
//type death
const deathsMask = 0xFF80;        // 0000 0000 0000 0000 1111 1111 1000 0000;
const parityMask = 0x80000000;    // 1000 0000 0000 0000 0000 0000 0000 0000;

let getParity = function (x) {
    x ^= x >> 16;
    x ^= x >> 8;
    x ^= x >> 4;
    x ^= x >> 2;
    x ^= x >> 1;
    return ((~x) & 1) << parityOffs;
};

let decodeMessage = function (message) {
    let x = (message & ~(parityMask));

    if ((message & parityMask) !== getParity(x)) {
        console.warn("Invalid parity " + message);
    }

    let type = (typeMask & message) >> typeOffs;
    let sig = {};
    switch (type) {
        case SIG_SETUP_FINISHED:
            console.log("Setup finished received");
            sig.type = SIG_TYPE.SETUP_FINISHED;
            break;
        case SIG_TIMER_STARTED:
            console.log("Timerstarted received");
            sig.type = SIG_TYPE.TIMER_STARTED;
            break;
        case SIG_GAME_OVER:
            console.log("Gameover received");
            sig.type = SIG_TYPE.GAME_OVER;
            break;
        case SIG_DEATH:
            console.log("Death received");
            sig.type = SIG_TYPE.DEATH;
            sig.victimID = (victimIDMask & message) >> victimOffs;
            sig.deaths = (deathsMask & message) >> deathOffs;
            break;
        case SIG_HIT:
            console.log("Radio Hit received");
            sig.type = SIG_TYPE.HIT;
            sig.offenderID = (offenderIDMask & message) >> offenderIDOffs;
            sig.kills = (killsMask & message) >> killOffs;
            sig.victimID = (victimIDMask & message) >> victimOffs;
            if ((hasAssistMask & message) >> hasAssistOffs) {
                sig.hasAssist = true;
                sig.assistID = (assistIDMask & message) >> assistIDOffs;
                sig.assists = (assistsMask & message) >> assistOffs;
            } else sig.hasAssist = false;
            break;
        default:
            console.log("Unknown received");
            sig.type = SIG_TYPE.UNKOWN;
            break;
    }
    return sig;
};

game_network.registerListener = function (listener) {
    game_network.signal_listener = listener;
};

game_network.startReceiving = function (pin) {
    rcswitch.enableReceive(pin);
    setInterval(function () {
        if (rcswitch.available()) { // Check if there is a pending code
            let value = rcswitch.getReceivedValue();
            rcswitch.resetAvailable(); // Reset `available` state
            let signal = decodeMessage(value);
            if (game_network.signal_listener) {
                game_network.signal_listener(signal);
            }
        }
    }, 500); // Set code checking interval
};

game_network.prepareTransmission = function (pin) {
    rcswitch.enableTransmit(pin);
};

game_network.sendMessage = function (type) {
    let word = (type << typeOffs);
    //word = word | getParity(word);
    let msg = word.toString(2).padStart(32, '0');
    console.log("Sending message " + msg);
    rcswitch.send(msg);
};


module.exports = game_network;
