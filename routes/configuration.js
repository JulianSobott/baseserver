var express = require('express');
var router = express.Router();

var game_setup = require('../lib/game_setup');
var game_manager = require('../lib/game_manager');

router.get('/*', function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next(); // http://expressjs.com/guide.html#passing-route control
});


router.get('/', function (req, res) {
    return res.json({"result": 0, "value": game_setup.setup});
});


router.post('/set/:param/', function (req, res) {
    let value = req.body.value;
    if (value !== undefined) {
        if (game_manager.isRegistration()) {
            let old = JSON.stringify(game_setup.setup);
            game_setup.setup[req.params.param] = value;
            if (!game_setup.isValid()) {
                game_setup.setup = JSON.parse(old);
                return res.json({"result": 3, "newvalue": game_setup.setup[req.params.param]});
            }
            res.json({"result": 0, "newvalue": game_setup.setup[req.params.param]});
            game_setup.send(); //Kind of unnecessary. But if multiple clients should be using the interface this is required
        } else {
            return res.json({"result": 2});
        }
    } else {
        return res.json({"result": 1});
    }
});

router.post('/player/set/:id', function (req, res) {
    let value = req.body;
    let id = req.params.id;
    if (value) {
        if (game_manager.isRegistration()) {
            if (value.id !== Number(id)) {
                return req.json({"result": 4});
            }
            let old = JSON.stringify(game_setup.setup);
            game_setup.setup.players[id] = value;
            if (!game_setup.isValid()) {
                game_setup.setup = JSON.parse(old);
                return res.json({"result": 3});
            }
            res.json({"result": 0});
            game_setup.send();
        } else {
            return res.json({"result": 2});
        }
    } else {
        return res.json({"result": 1});
    }
});

router.get('/player/remove/:id', function (req, res) {
    let id = req.params.id;

    if (game_manager.isRegistration()) {
        if (!game_setup.setup.players[id]) {
            return res.json({"result": 3});
        }
        delete game_setup.setup.players[id];
        res.json({"result": 0});
        game_setup.send();
    } else {
        return res.json({"result": 2});
    }

});





module.exports = router;
