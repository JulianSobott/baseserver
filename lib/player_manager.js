let player_manager = this;

var websockets = require('../lib/websockets');

const PHASES = {"REGISTRATION": 0, "READY": 1, "RUNNING": 2, "EVALUATION": 3};

let current_phase = PHASES.REGISTRATION;
/*
 List of players ids which is populated during registration phase
 */
let registered = [];
/*
 List of player ids which still have to send their ready signal. Populated once ready
 */
let tobeready = [];

/*
Active player ids. Populated once running
 */
let active = [];

/*
List of player ids which still have to submit their final result. Populated once evaluation phase started
 */
let tobeevaluated = [];

let registrationCallback;


player_manager.getPlayerLists = function () {
    return {
        "registered": registered,
        "tobeready": tobeready,
        "active": active,
        "tobeevaluated": tobeevaluated
    };
};

player_manager.getRegisteredPlayers = function () {
    return registered;
};

player_manager.isRegistered = function (id) {
    return registered.includes(id);
};

let sendPlayerLists = function () {
    let msg = {"type": "player_lists"};
    msg.value = player_manager.getPlayerLists();
    websockets.broadcast(JSON.stringify(msg));
};

let sendPhase = function () {
    let msg = {"type": "player_manager_phase"};
    msg.value = player_manager.getPhaseString();
    websockets.broadcast(JSON.stringify(msg));
    sendPlayerLists();
};

player_manager.getPhaseString = function () {
    return Object.keys(PHASES)[current_phase];
};

player_manager.setRegistrationCallback = function (callback) {
    registrationCallback = callback;
};

player_manager.setReady = function () {
    tobeready = registered.slice();
    current_phase = PHASES.READY;
    sendPhase();
};

player_manager.allReady = function () {
    return tobeready.length === 0;
};

player_manager.startGame = function () {
    active = registered.slice();
    current_phase = PHASES.RUNNING;
    sendPhase();
};

player_manager.finishGame = function () {
    tobeevaluated = active.slice();
    active = [];
    current_phase = PHASES.EVALUATION;
    sendPhase();
};

player_manager.allEvaluated = function () {
    return tobeevaluated.length === 0;
};

player_manager.removePlayer = function (id) {
    let i = registered.indexOf(id);
    if (i !== -1) {
        registered.splice(i, 1);
    }
    i = tobeready.indexOf(id);
    if (i !== -1) {
        tobeready.splice(i, 1);
    }
    i = active.indexOf(id);
    if (i !== -1) {
        active.splice(i, 1);
    }
    i = tobeevaluated.indexOf(id);
    if (i !== -1) {
        tobeevaluated.splice(i, 1);
    }
    sendPlayerLists();
};

player_manager.registerPlayer = function (id) {
    if (current_phase === PHASES.REGISTRATION) {
        if (registered.includes(id)) {
            console.log("Player " + id + " already registered");
        } else {
            registered.push(id);
            if (registrationCallback) {
                registrationCallback(id);
            }
            sendPlayerLists();
        }
        return true;
    }
    return false;
};

player_manager.readyPlayer = function (id) {
    if (current_phase === PHASES.READY) {
        if (registered.includes(id)) {
            let i = tobeready.indexOf(id);
            if (i === -1) {
                console.log("Player " + id + " already ready");
            } else {
                tobeready.splice(i, 1);
                sendPlayerLists();
            }
            return true;
        } else {
            console.error("Cannot ready player " + id + " because they are not registered");
        }
    }
    return false;
};

player_manager.evaluatePlayer = function (id) {
    if (current_phase === PHASES.EVALUATION) {
        if (registered.includes(id)) {
            let i = tobeevaluated.indexOf(id);
            if (i === -1) {
                console.log("Player " + id + " already evaluated");
            } else {
                tobeevaluated.splice(i, 1);
                sendPlayerLists();
            }
            return true;
        }
    }
    return false;
};


module.exports = player_manager;