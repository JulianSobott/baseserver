let game_manager = this;

let game_state = require('../lib/game_state');
let game_network = require('../lib/game_network');
let game_setup = require('../lib/game_setup');
let player_manager = require('../lib/player_manager');
var websockets = require('../lib/websockets');


const PHASES = {"REGISTRATION": 0, "READY": 1, "RUNNING": 2, "EVALUATION": 3};
game_manager.PHASES = PHASES;

let current_phase = PHASES.REGISTRATION;

game_manager.getPhase = function () {
    return current_phase;
};

game_manager.getPhaseString = function () {
    return Object.keys(PHASES)[current_phase];
};

let sendPhase = function () {
    let msg = {"type": "game_manager_phase"};
    msg.value = game_manager.getPhaseString();
    websockets.broadcast(JSON.stringify(msg));
};

game_manager.isRegistration = function () {
    return current_phase === PHASES.REGISTRATION;
};

game_manager.isReady = function () {
    return current_phase === PHASES.READY;
};

game_manager.isRunning = function () {
    return current_phase === PHASES.RUNNING;
};

game_manager.isEvaluating = function () {
    return current_phase === PHASES.EVALUATION;
};

game_manager.isPrepared = function () {
    if (current_phase === PHASES.REGISTRATION) {
        let players = player_manager.getRegisteredPlayers();
        for (let i; i < players.length; i++) {
            let id = players[i];
            if (!game_setup.setup.players.hasOwnProperty("" + id)) {
                console.warn("Registered player " + id + " cannot be found in game setup");
                return false;
            }
        }
        return true;
    }
    return false;
};

game_manager.setReady = function () {
    if (game_setup.isValid() && game_manager.isPrepared()) {
        player_manager.setReady();
        current_phase = PHASES.READY;
        sendPhase();
        game_setup.store();
        return true;
    }

    return false;
};

game_manager.canStart = function () {
    return current_phase === PHASES.READY && player_manager.allReady();
};

game_manager.startGame = function () {
    if (game_manager.canStart()) {
        player_manager.startGame();
        let player_list = {};
        for (let [id, value] of Object.entries(game_setup.setup.players)) {
            player_list[value.id] = {"name": value.name, "team": value.team, "id": value.id};
        }
        let stoptime;
        let stopscore;
        if (game_setup.setup.stopTrigger === "time") {
            stoptime = game_setup.setup.timeLimit * 60 * 1000;
        } else if (game_setup.setup.stopTrigger === "score") {
            stopscore = game_setup.setup.scoreLimit;
        }
        game_state.startGame(player_list, stopscore, stoptime);
        current_phase = PHASES.RUNNING;
        game_network.sendMessage(game_network.SIG_TYPE.TIMER_STARTED);
        setTimeout(function () {
            game_network.sendMessage(game_network.SIG_TYPE.TIMER_STARTED);
        }, 1000);
        setTimeout(function () {
            game_network.sendMessage(game_network.SIG_TYPE.TIMER_STARTED);
        }, 2000);
        sendPhase();
        return true;
    }
    return false;
};


let onNewPlayer = function (id) {
    if (current_phase === PHASES.REGISTRATION) {
        if (!game_setup.setup.players.hasOwnProperty("" + id)) {
            game_setup.setup.players["" + id] = {"name": "Player" + id, "id": id, "team": 0};
        }
    }
};

player_manager.setRegistrationCallback(onNewPlayer);

game_manager.processRadio = function (signal) {
    console.log("Received signal" + signal);
    switch (signal.type) {
        case game_network.SIG_TYPE.DEATH:
            game_state.processDeath(signal);
            break;
        case game_network.SIG_TYPE.HIT:
            game_state.processHit(signal);
            break;
        default:
            console.log("Unknown signal received");
    }
};

game_manager.handleResult = function (id, results) {
    player_manager.evaluatePlayer(id);
    game_state.processResult(String(id), results);
};

game_manager.startTestGame = function () {
    if (!game_state.isNotStarted()) {
        console.log("Already running");
        return false
    }
    if (game_setup.isValid()) {
        let player_list = {};
        for (let [id, value] of Object.entries(game_setup.setup.players)) {
            player_list[value.id] = {"name": value.name, "team": value.team, "id": value.id};
        }
        game_state.startGame(player_list);
        return true;
    } else {
        console.warn("Game setup is not valid");
        return false;
    }
};

game_manager.finishGame = function () {
    if (game_manager.isRunning()) {
        player_manager.finishGame();
        game_state.stopGame();
        current_phase = PHASES.EVALUATION;
        sendPhase();
        return true;
    }
    return false;
};

let tick = function () {
    if (game_manager.isReady() && !player_manager.allReady()) {
        game_network.sendMessage(game_network.SIG_TYPE.SETUP_FINISHED);

    } else if (game_manager.isRunning()) {
        if (!game_state.tick()) {
            game_manager.finishGame();
        }
    } else if (game_manager.isEvaluating() && !player_manager.allEvaluated()) {
        game_network.sendMessage(game_network.SIG_TYPE.GAME_OVER);
    }
};
setInterval(tick, 1000);

module.exports = game_manager;